# MStream

[MStream](https://www.mstream.io) All your music, everywhere you go.

## Access

It is available at [https://mstream.{{ domain }}/](https://mstream.{{ domain }}/) or [http://mstream.{{ domain }}/](http://mstream.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://mstream.{{ tor_domain }}/](http://mstream.{{ tor_domain }}/)
{% endif %}
